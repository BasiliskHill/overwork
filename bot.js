//  Dependancies
const Eris = require("eris");
const fs = require("fs");
const config = require("./config.json");

//  Config values
config.prefix = config.prefix ? config.prefix : "w.";
config.commandDir = config.commandDir ? config.commandDir : "./commands";
config.functionDir = config.functionDir ? config.functionDir : "./functions";
config.logDir = config.logDir ? config.logDir : "./logs";
config.logMax = config.logMax ? config.logMax : 5;
config.embedColour = config.embedColour ? config.embedColour : "0x193187";

//  Create the functions list
const funcLoader = require(`${config.functionDir}/funcLoader`).execute;
let functions = funcLoader(config.functionDir);

//  Bot Object
const bot = new Eris.Client(config.token);

//  Variables
let commands = {};
let aliases = {};

log = functions.log.init(config);

//  Command List
//  Iterate over the original command directory
for (file of fs.readdirSync(config.commandDir, {withFileTypes: true})) {
    //  If it's a file, assume it's a command
    if (file.isFile()) {
        //  Require in the command and add it to the command object
        command = require(`${config.commandDir}/${file.name}`)
        commands[command.name] = command;

        //  Add each alias to the aliases obj
        if (command.aliases != undefined) {
            for (alias of command.aliases) {
                aliases[alias] = command.name;
            }
        }
    }
    //  If it's a directory, 
    else if(file.isDirectory()) {
        commands[file.name] = parentDefault(file);
        //  Call the command loader on the new directory
        cmdLoader(`${config.commandDir}/${file.name}`, commands[file.name], aliases);
    }
}

//  Command Handler
bot.on("messageCreate", (msg) => {
    //  Remove the config.prefix and move it to an array
    content = msg.content.toLowerCase().slice(config.prefix.length).split(" ");

    //  Create the cmdCall variable as an output
    let cmdCall;
    //  Create a variable to hold the last list of commands to check
    let prevCmd = commands;
    for (param of content) {
        if (prevCmd.hasOwnProperty(param)) {
            //  Set the command to call after the loop to that param
            cmdCall = prevCmd[param];
            //  Change to the current command list
            prevCmd = prevCmd[param].children;
        }
        else if (aliases.hasOwnProperty(param)) {
            //  Set the command to call after the loop to that pointed to by the alias
            cmdCall = prevCmd[aliases[param]];
            //  Change to the current command list, as pointed to by the alias
            prevCmd = prevCmd[aliases[param]].children;
        }
    }
    //  Run the command, if it exists
    if (cmdCall != undefined) cmdCall.execute(msg, config);
})

//  Main Bot Functions

//  Command Loader (after the first addition)
function cmdLoader(dir, parent, aliases) {
    //  Iterate over the directory
    for (file of fs.readdirSync(dir, {withFileTypes: true})) {
        //  The current directory of the file
        const fileDir = `${dir}/${file.name}`;

        //  If it's a file
        if (file.isFile()) {
            //   and not an index
            if (file.name != "index.js") {
                //  Require in the command and add it to the parent
                command = require(fileDir);
                if (parent.children === undefined) parent.children = {};
                parent.children[command.name] = command;

                //  Add each alias to the aliases obj
                for (alias of command.aliases) {
                    aliases[alias] = command.name;
                }
            }
            //  Otherwise, it must be an index
            else {
                //  Read the index file
                const index = require(fileDir);
                //  Alter the parent
                for (option in index) {
                    parent[option] = index[option];
                }
            }  
        }
        //  If it's a directory, 
        else if(file.isDirectory()) {
            parent.children[file.name] = parentDefault(file);
            //  Call the command loader on the new directory
            cmdLoader(`${config.commandDir}/${file.name}`, commands[file.name], aliases);
        }
    }
}

//  Default parent info
function parentDefault(file) {
    return {
        name: file.name,
        description: `The \`\`${file.name}\`\` parent command`,
        execute(msg, config) {
            //  Get a date object
            const date = new Date();
            //  Get the command name
            const cmdName = this.name;
            //  Create a children array / embed field array
            const cmdChildren = [];

            //  If the parent has children
            if (this.children != undefined) {
                console.log(this.children);
                //  Loop over the children object of the command
                for (child in this.children) {
                    //  Push the field object to the command's children array
                    cmdChildren.push({
                        name: `**${this.children[child].name}**`,
                        value: this.children[child].description
                    });
                }
            }

            msg.channel.createMessage({
                embed: {
                    title: "**HELP!**",
                    description: `The \`\`${cmdName}\`\` parent command`,
                    fields: cmdChildren,
                    timestamp: date.toISOString(),
                    footer: {
                        text: `Requested by ${msg.author.username}`,
                        icon_url: msg.author.avatarURL
                    },
                    color: parseInt(config.embedColour)
                }
            });
        }
    }
}

//  Connect to Discord
bot.connect();