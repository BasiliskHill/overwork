module.exports = {
    name: "ping",
    description: "Pong!",
    aliases: ["p"],
    execute: function(msg, config) {
        //  Create a date object
        const date = new Date();
        //  Create responses array
        const responses = ["Pang!", "Peng!", "Pong!", "Pung!"];

        //  Send the embed
        msg.channel.createMessage({
            embed: {
                title: "Pong!",
                description: `:ping_pong: ${responses[Math.floor(Math.random() * responses.length)]}`,
                timestamp: date.toISOString(),
                footer: {
                    text: `Requested by ${msg.author.username}`,
                    icon_url: msg.author.avatarURL
                },
                color: parseInt(config.embedColour)
            }
        });
    }
}