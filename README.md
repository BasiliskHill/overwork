# Overwork
A reboot of the old and deprecated Partius bot.\
Overwork will continue to be made in nodeJS using the eris library, but will move to its own command handler.

## Format
This section will be updated with more details concerning command, config, and file system formatting as each occurs.

### Config
All config for the bot is handled by a config.json file in the working directory.

__attributeName__ (dataType) [Default Value]: Description of the attribute\
(Please note that the capitalisation of the attribute name does matter)

__token__ (string) [Required]: The token the bot client uses to connect to the bot\
__name__ (string) ["Overwork"]: Name of the bot (set by editSelf on startup)\
__avatar__ (string) ["./avatar.png"]: Location of the avatar's image file (used to set avatar via editSelf on startup)\
__prefix__ (string) ["w."]: Prefix used by the bot\
__commandDir__ (string) ["./commands"]: Location of the modular commands directory\
__functionDir__ (string) ["./functions"]: Location of the non-modular, vital functions directory\
__logDir__ (string) ["./logs"]: Location of the logging directory\
__logMax__ (int) [5]: The maximum number of logs to be stored in the logging directory\
__embedColour__ (string) ["0x193187"]: Used as the default colour for every embed

### Commands
All commands are to be placed within a separate commands directory, configurable in the config.\
The commands, and command loader designed to use them, are completely modular, so anything within the command file may be removed.

__attributeName__ (dataType) [Default Value]: Description of the attribute\
(Please note that the capitalisation of the attribute name does matter)

__name__ (string) [Required]: The name of the command, used by the command handler\
__description__ (string) [null]: The description of the command, used in help texts\
__aliases__ (array of strings) [null]: A list of aliases, additional names that point to the original command\
__execute__ (function) [Required]: The method run when the command is called\
Dynamically loaded on boot (not configurable or editable)\
__children__ (object) [Dynamic]: A list of a parent command's children

### Functions
Unlike commands, functions are not modular and are required for specific functions of the bot and as such aren't removable.

__attributeName__ (dataType) [Default Value]: Description of the attribute\
(Please note that the capitalisation of the attribute name does matter)

__name__ (string) [Required]: The name of the function, what's used to access the execute method
__execute__ (function) [Required]: The method exectuted when the function is called

