module.exports = {
    name: "err",
    execute: function (log, msg, err) {
        fs = require("fs");

        //  Create a date object
        date = new Date();
        //  Format the timestamp
        timestamp = `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;

        //  If the log is non-null, assume we've been passed a valid location to log to
        if (log != null) {
            //  Add onto the log file
            fs.appendFile(log, `\n[${timestamp}] [ERROR]: ${msg}\n${err.stack}`, (e) => {
                if (e) throw e;
            })
        }
        //  Otherwise, assume there is no log
        else {
            console.log(`[LOG FILE MISSING] [${timestamp}] [ERROR]: ${msg}\n${err.stack}`);
        }
    }
}