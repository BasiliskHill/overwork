//  Init can either return null, or the directory for the log file

module.exports = {
    name: "init",
    execute: function(config) {
        const fs = require("fs");

        try {
            //  Attempt to read the logs directory
            logs = fs.readdirSync(config.logDir);
        }
        catch (err) {
            //  If the directory doesn't exist
            if (err.code == "ENOENT") {
                //  Print to console that log directory cannot be found
                console.log(`[WARN]: Logging directory (${config.logDir}) cannot be found`);
                console.log("[INFO]: Attempting to create logging directory");

                try {
                    //  Attempt to make the logging directory
                    fs.mkdirSync(config.logDir);
                    
                    //  Set logs to the new directory
                    logs = fs.readdirSync(config.logDir);
                }
                catch (e) {
                    //  If the program doesn't have permission to make the directory
                    if (e.code == "EPERM") {
                        //  Print to console that no log directory can be made
                        console.log("[ERROR]: INVALID PERMISSIONS TO CREATE LOGGING DIRECTORY");

                        //  Return null
                        return null;
                    }
                    //  Throw any other error
                    else throw e;
                }
            }
            //  Throw any other error
            else throw err;
        }

        //  If the logs directory already has the maximum number of logs
        if(logs.length == config.logMax) {
            //  Create the oldestLog object for the loop to dump information into
            let oldestLog = {
                name: "",
                birth: 0
            };
            //  Iterate over the logs
            for (log of logs) {
                //  Get the log's birth time
                let newBirth = fs.statSync(`${config.logDir}/${log}`).birthtimeMs;
                
                //  If the log is older than the oldest log or the oldestLog hasn't been updated
                if (newBirth < oldestLog.birth || oldestLog.birth == 0) {
                    //  Update the oldestLog
                    oldestLog.name = log;
                    oldestLog.birth = newBirth;
                }
            }

            
            try {
                //  Remove the oldest log
                fs.unlinkSync(`${config.logDir}/${oldestLog.name}`);
            }
            catch (err) {
                if (err.code == "EPERM") {
                    //  Log the error
                    console.log("[ERROR]: INVALID PERMISSIONS TO REMOVE OLD LOG FILE");

                    //  Return null
                    return null;
                }
                else throw err;
            }
        }
        //  Create a date object to use in the file name
        let date = new Date();
        //  Get the isoString
        let isoDate = date.toISOString();
        //  Format the date string
        logName = `${isoDate.slice(0, isoDate.indexOf("T"))}-`;

        //  Find a valid unique identifier for the log
        let UID = 0;

        while (logs.includes(logName + String(UID))) {
            UID++;
        }

        try {
            //  Create the new log file
            let timestamp = `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;
            fs.writeFileSync(`${config.logDir}/${logName + String(UID)}`, `[${timestamp}] [INFO]: LOG INIALISED`);
        }
        catch (err) {
            if (err.code == "EPERM") {
                //  Log the error
                console.log("[ERROR]: INVALID PERMISSIONS TO CREATE NEW LOG FILE");

                //  Return null
                return null;
            }
        }

        //  Return the log directory
        return `${config.logDir}/${logName + String(UID)}`;

    }
}