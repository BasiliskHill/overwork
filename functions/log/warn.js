module.exports = {
    name: "warn",
    execute: function (log, msg) {
        fs = require("fs");

        //  Create a date object
        date = new Date();
        //  Format the timestamp
        timestamp = `${date.getHours()}:${date.getMinutes()}:${date.getSeconds()}`;

        //  If the log is non-null, assume we've been passed a valid location to log to
        if (log != null) {
            //  Add onto the log file
            fs.appendFile(log, `\n[${timestamp}] [WARN]: ${msg}`, (err) => {
                if (err) throw err;
            })
        }
        //  Otherwise, assume there is no log
        else {
            console.log(`[LOG FILE MISSING] [${timestamp}] [WARN]: ${msg}`);
        }
    }
}