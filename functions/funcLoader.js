module.exports = {
    name: "funcLoader",
    execute: funcLoader
}

//  Function Loader
function funcLoader (dir, parent) {
    fs = require("fs");

    //  If a null parent is passed, assume it's the start of the loop and set parent to an empty object.
    if (parent == null) parent = {}

    //  Iterate over the function directory
    for (file of fs.readdirSync(dir, {withFileTypes: true})) {
        //  If the file object is a file
        if (file.isFile() && file.name != "funcLoader.js") {
            //  Require the function and add it to the parent
            func = require(`.${dir.slice(11)}/${file.name}`);
            parent[func.name] = func.execute;
        }

        //  If the file object is a directory
        else if (file.isDirectory()) {
            //  Add an empty object to the parent
            parent[file.name] = {};

            //  Call funcLoader on the directory minus /functions
            funcLoader(`${dir}/${file.name}`, parent[file.name]);
        }
    }

    //  Return the original object
    return parent;
}